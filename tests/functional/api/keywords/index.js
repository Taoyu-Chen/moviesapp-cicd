import chai from "chai";
import request from "supertest";

const expect = chai.expect;

let api;

const sampleMovieId = 424;
const sampleKeywordId = 818;

describe("Keywords endpoint", () => {
  beforeEach(async () => {
    try {
      api = require("../../../../index");
    } catch (err) {
      console.error(`failed to Load user Data: ${err}`);
    }
  });
  after(() => {
    api.close(); // Release PORT 8080
    delete require.cache[require.resolve("../../../../index")];
  });
  describe("GET /keywords ", () => {
    it("should return 19 keywords and a status 200", (done) => {
      request(api)
        .get(`/api/keywords/${sampleMovieId}`)
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          expect(res.body).to.be.a("array");
          expect(res.body.length).to.equal(19);
          
          setTimeout(done, 10000);
        });
    });
  });

  describe("GET /keywords/test/:id", () => {
    describe("when the id is valid", () => {
      it("should return the matching movie keyword", () => {
        return request(api)
          .get(`/api/keywords/test/${sampleKeywordId}`)
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .then((res) => {
            expect(res.body).to.have.property("name","based on novel or book");
          });
      });
    });
    describe("when the id is invalid", () => {
      it("should return the NOT found message", () => {
        return request(api)
          .get("/api/movies/xxx")
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect({
            success: false,
            status_code: 34,
            status_message: "The resource you requested could not be found.",
          });
      });
    });
  });
});
