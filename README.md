# Assignment 2 - Agile Software Practice.

Name: Taoyu Chen

## Target Web API

- Get /api/movies - return an array of movie objects.
- Get /api/movies/:id - return detailed information on a specific movie.
- Get /api/keywords/:id - return an array of a specific movie keywords objects.
- Get /api/keywords/test/:id - return a specific movie keyword objects. **After a certain point in time, this test cannot be used. If I use it.only() function, it will run normally, and the test will cause an error.**
- Get /api/toprated/ - return an array of toprated movie objects.
- POST /api/toprated/ - add a new movie to toprated movies database, and return updated movie. 
- GET /api/toprated/:id - return a toprated movie in database. **After a certain point in time, this test cannot be used. If I use it.only, it will run normally, and the test will cause an error. And will detect legal input.**
- Put /api/toprated/:id - update a specific toprated movie title and return updated movie. 
- Delete /api/toprated/:id - delete a specific toprared movie by id.
- Get /api/similar/:id - return an array of similar movie objects.
- POST /api/similar/ - add a new movie to similar movies database, and return updated movie. 
- GET /api/similar/movie/:id - return a similar movie in database. **After a certain point in time, this test cannot be used. If I use it.only, it will run normally, and the test will cause an error. And will detect legal input.**
- Put /api/similar/:id - update a specific similar movie title and return updated movie. 
- Delete /api/similar/:id - delete a specific similar movie by id.

## Error/Exception Testing.

- Get /api/movies/:id - test whether the id is a legal number, and if it is illegal, receive the status code information

  **tests/functional/api/movies/index.js**

- Get /api/toprated/:id - test if the movie to which id belongs is in the database, if not, receive error 404.

  **tests/functional/api/toprated/index.js**

- PUT /api/toprated/:id - test whether the updated movie title is updated. If the movie ID you want to update is not in the database, accept an error 404. And verify the movie after the update.

  **tests/functional/api/toprated/index.js**

- DELETE /api/toprated/:id - Test whether the movie to which the ID belongs is in the database, if not, receive an error 404, if it is, delete and return success, and test whether the deleted movie has been deleted cleanly after the deletion is successful

  **tests/functional/api/toprated/index.js**

- Delete /api/toprated/:id - test when the id is not a number, is a number but cannot be found in database. Test deleting a movie without prior authentication. See tests/functional/api/upcomings/index.js

- Get /api/similar/movie/:id - test if the movie to which id belongs is in the database, if not, receive error 404.

  **tests/functional/api/similar/index.js**

- PUT /api/similar/:id - test whether the updated movie title is updated. If the movie ID you want to update is not in the database, accept an error 404. And verify the movie after the update.

  **tests/functional/api/similar/index.js**

- DELETE /api/similar/:id - Test whether the movie to which the ID belongs is in the database, if not, receive an error 404, if it is, delete and return success, and test whether the deleted movie has been deleted cleanly after the deletion is successful

  **tests/functional/api/similar/index.js**

## Continuous Delivery/Deployment.

Specify the URLs for the staging and production deployments of your web API

- https://movies-api-staging-20091612.herokuapp.com/ - Staging deployment
- https://moviesapp20091612.herokuapp.com/ - Production

Show a screenshot

![][Heroku]



## Independent learning (If relevant).

[Heroku]: ./public/heroku.png

