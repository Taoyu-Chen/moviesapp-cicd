web: node build/index.js

| Name                   | GET                                       | POST | PUT  | DELETE |
| ---------------------- | ----------------------------------------- | ---- | ---- | ------ |
| /api/keywords/:id      | Get and store a moive keywords to mongoDB |      |      |        |
| /api/keywords/test/:id | Get a moive keyword by ID                 |      |      |        |